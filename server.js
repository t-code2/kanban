const express = require('express');
const bodyParser = require('body-parser');
const { Server } = require("socket.io");
const http = require('http')
const app = express();
const port = 4444;
const server = http.createServer(app)
const io = new Server(server);

app.use(express.static('dist'));
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.post('/', (req, res) => {
  const data = req.body
  console.log(data)
  switch (data.event) {
    case 'ONTASKADD':
      io.emit('taskAdd', {
        taskId: data.data.FIELDS_AFTER.ID
      })
      break;
    case 'ONTASKUPDATE':
      io.emit("taskUpdate", {
        taskId: data.data.FIELDS_AFTER.ID,
      })
      break;
    case 'ONTASKCOMMENTADD':
    case 'ONTASKCOMMENTUPDATE':
      io.emit("commentUpdate", {
        targetEvent: 'COMMENT',
        event: data.event.slice(13),
        taskId: data.data.FIELDS_AFTER.TASK_ID,
      })
      break;
  }
})


server.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})