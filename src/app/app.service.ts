import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { firstValueFrom } from 'rxjs'
import { BitrixAnswer, CommentTask, CreateCommentObject, CreateTaskObject, Project, Task, UpdateTaskObject, User } from './app.types'
import { transformTaskStatus } from './utils/transformTaskStatus'

@Injectable({
  providedIn: 'root',
})
export class AppService {
  private readonly rootApi = 'https://truecode.bitrix24.ru/rest/591/k9oqevn5ogsedo03/'

  constructor(private httpClient: HttpClient) {}

  async getTaskById(taskId: string): Promise<Task> {
    const data = await firstValueFrom(this.httpClient.get<BitrixAnswer>(
      this.rootApi + 'tasks.task.get',
      {
        params: {
          taskId
        }
      }
    ))
    const taskRaw = (data.result as any).task;
    const task: Task = {
      id: taskRaw['id'],
      title: taskRaw['title'],
      description: taskRaw['description'],
      status: transformTaskStatus(taskRaw['status']),
      responsible: taskRaw['responsible'].name,
      projectName: taskRaw['group'].name,
    }
    return task;
  }

  async getTasks(projectId: string): Promise<Task[]> {
    const tasks: Task[] = [];
    let isGetAll = false;
    let nextId = 0;

    while (!isGetAll) {
      const data = await firstValueFrom(this.httpClient.get<BitrixAnswer>(this.rootApi + 'tasks.task.list', {
        params: {
          'filter[GROUP_ID]': projectId,
          start: nextId
        }
      }))
      if ((data.result as any).tasks.length < 50) {
        isGetAll = true
      }
      nextId += 50
      
      tasks.push(
        ...(data.result as any).tasks.map((taskRaw: any) => ({
          id: taskRaw['id'],
          title: taskRaw['title'],
          description: taskRaw['description'],
          status: transformTaskStatus(taskRaw['status']),
          responsible: taskRaw['responsible'].name,
          projectName: taskRaw['group'].name,
        }))
      )
    }
    return tasks
  }

  async getProjects(): Promise<Project[]> {
    const projects: Project[] = [];
    let isGetAll = false;
    let nextId = 0;

    while (!isGetAll) {
      const data = await firstValueFrom(this.httpClient.get<BitrixAnswer>(this.rootApi + 'sonet_group.get.json', {
        params: {
          start: nextId
        }
      }))
      if (data.result.length < 50) {
        isGetAll = true
      }
      nextId += 50
      projects.push(
        ...data.result.map(projectRaw => ({
          id: projectRaw['ID'],
          title: projectRaw['NAME']
        }))
      )
    }
    return projects
  }

  async createComment(taskId: number, commentField: CreateCommentObject) {
    await firstValueFrom(this.httpClient.post(
      this.rootApi + 'task.commentitem.add',
      {
        TASKID: taskId,
        FIELDS: commentField
      }
    ));
  }

  async createTask(task: CreateTaskObject) {
    await firstValueFrom(this.httpClient.post(
      this.rootApi + 'tasks.task.add',
      {
        'fields': task
      }
    ))
  }

  async updateTask(taskId: number, updateTask: UpdateTaskObject) {
    await firstValueFrom(this.httpClient.post(
      this.rootApi + 'tasks.task.update',
      {
        'taskId': taskId.toString(),
        'fields': updateTask
      }
    ));
  }

  async getUsers() {
    const users: User[] = [];
    let isGetAll = false;
    let nextId = 0;

    while (!isGetAll) {
      const data = await firstValueFrom(this.httpClient.post<BitrixAnswer>(
        this.rootApi + '/user.get', 
        {
          filter: {
            ACTIVE: true
          }
        },
        {
          params: {
            start: nextId,
          },
        }))
      if (data.result.length < 50) {
        isGetAll = true
      }
      nextId += 50
      
      users.push(
        ...data.result.map(userRaw => {
          const userName = userRaw['NAME'] && userRaw['LAST_NAME'] ? `${userRaw['NAME']} ${userRaw['LAST_NAME']}` : userRaw['EMAIL']
          return {
            id: userRaw['ID'],
            name: userName
          };
        })
      )
    }
    return users
  }

  async getTaskComments(taskId: number) {
    const comments: CommentTask[] = [];

    const data = await firstValueFrom(
      this.httpClient.get<BitrixAnswer>(
        this.rootApi + 'task.commentitem.getlist',
        {
          params: { TASKID: taskId },
        },
      ),
    );
    
    comments.push(
      ...data.result.map(taskRawComment => ({
        author: taskRawComment['AUTHOR_NAME'],
        message: taskRawComment['POST_MESSAGE'],
        date: new Date(Date.parse(taskRawComment['POST_DATE']))
      }))
    )

    comments.sort((commentA, commentB) => {
      if (commentA.date > commentB.date) return 1
      else return -1;
    })

    return comments;
  }
}