import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainViewComponent } from './pages/main-view/main-view.component'
import { ProjectsComponent } from './pages/projects/projects.component'

const routes: Routes = [
  { path: '', component: ProjectsComponent },
  { path: 'project/:id', component: MainViewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
