import { SocketIoConfig } from 'ngx-socket-io'

const config: SocketIoConfig = {
  url: 'ws://188.225.27.209:4444'
}

export {
  config
}