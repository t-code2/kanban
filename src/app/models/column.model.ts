import { Task } from '../app.types'

export class Column {
  constructor(public name: string, public tasks: Task[]) {}
}