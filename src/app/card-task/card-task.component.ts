import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import { Task } from '../app.types'
import { DialogAnimationsExampleDialog } from './card-full.component'

@Component({
  selector: 'app-card-task',
  templateUrl: './card-task.component.html',
  styleUrls: ['./card-task.component.scss']
})
export class CardTaskComponent {
  @Input() title = 'Название задачи';
  @Input() description = 'dkfjskjfksdjfk';
  @Input() task: Task | null = null;

  constructor(public dialog: MatDialog) {}

  openDialog(enterAnimationDuration: string, exitAnimationDuration: string): void {
    this.dialog.open(DialogAnimationsExampleDialog, {
      width: '800px',
      enterAnimationDuration,
      exitAnimationDuration,
      data: {
        title: this.task?.title,
        description: this.task?.description,
        id: this.task?.id,
      }
    });
  }
}
