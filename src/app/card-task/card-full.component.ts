import { AfterViewInit, Component, Inject, Input, OnInit } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'
import { Socket } from 'ngx-socket-io'
import { AppService } from '../app.service'
import { CommentTask } from '../app.types'
@UntilDestroy()
@Component({
  selector: 'dialog-animations-example-dialog',
  templateUrl: 'card-full.component.html',
  styleUrls: ['./card-full.component.scss']
})
export class DialogAnimationsExampleDialog implements OnInit, AfterViewInit {
  id: string = ''
  title: string = ''
  description: string = ''
  comments: CommentTask[] = []
  textComment: string = ''

  constructor(
    public dialogRef: MatDialogRef<DialogAnimationsExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private appService: AppService,
    private socket: Socket,
  ) {}

  ngOnInit() {
    this.title = this.data.title
    this.description = this.data.description
    this.id = this.data.id

    this.socket.fromEvent('commentUpdate').pipe(
      untilDestroyed(this)
    ).subscribe(async (obj: any) => {
      this.comments = await this.appService.getTaskComments(+obj.taskId)
    })
  }

  async ngAfterViewInit() {
    this.comments = await this.appService.getTaskComments(+this.id)
  }

  async createComment() {
    if (!this.textComment.trim()) return

    await this.appService.createComment(+this.id, {
      POST_MESSAGE: this.textComment.trim()
    })
    this.textComment = '';
  }
}
