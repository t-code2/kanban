import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { Board } from 'src/app/models/board.model'
import { Column } from 'src/app/models/column.model'
import { ActivatedRoute } from '@angular/router'
import { AppService } from 'src/app/app.service'
import { Task } from 'src/app/app.types'
import { transformTaskStatusToNumber } from 'src/app/utils/transformTaskStatus'
import { MatDialog } from '@angular/material/dialog'
import { ModalAddTaskComponent } from 'src/app/modal-add-task/modal-add-task.component'
import { Socket } from 'ngx-socket-io'
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy'

@UntilDestroy()
@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss']
})
export class MainViewComponent implements OnInit {
  board: Board = new Board('Test board', [
    new Column('Ждёт выполнения', []),
    new Column('Выполняется', []),
    new Column('Ожидает контроля', []), 
    new Column('Завершена', []),
    new Column('Отложена', [])
  ]);

  projectName: string = 'Project name';

  private id: string;

  constructor(
    private activateRoute: ActivatedRoute,
    public dialog: MatDialog,
    private appService: AppService,
    private socket: Socket,
  ) {    
    this.id = this.activateRoute.snapshot.params['id'];
  }

  async ngOnInit() {
    await this.fetchTasks()

    this.socket.fromEvent('taskUpdate').pipe(
      untilDestroyed(this)
    ).subscribe(async (object: any) => {
      console.log('TASK UPDATE')
      const taskId = object.taskId
      const taskNew = await this.appService.getTaskById(taskId)
      for (const column of this.board.columns) {
        const updatedTask = column.tasks.findIndex(task => task.id === taskNew.id)
        if (updatedTask !== -1) {
          column.tasks.splice(updatedTask, 1)
        }
      }
      this.board.columns.find((column) => column.name === taskNew.status)?.tasks.push(taskNew)
    })
    this.socket.fromEvent('taskAdd').pipe(
      untilDestroyed(this)
    ).subscribe(async (object: any) => {
      const taskId = object.taskId
      const taskNew = await this.appService.getTaskById(taskId)
      this.board.columns[0].tasks.push(taskNew)
    })
  }

  async fetchTasks() {
    const tasks = await this.appService.getTasks(this.id);
    this.board.columns.forEach((column) => column.tasks = [])
    tasks.forEach((task) => {
      this.board.columns.find((column) => column.name === task.status)?.tasks.push(task)
    })
    this.projectName = tasks[0].projectName 
  }

  drop(event: CdkDragDrop<Task[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
      const task = event.container.data[event.currentIndex]
      const statusCode = transformTaskStatusToNumber(event.container.element.nativeElement.dataset['index']!)
      this.appService.updateTask(+task.id, {
        STATUS: statusCode
      })
    }
  }

  openDialog(enterAnimationDuration: string, exitAnimationDuration: string): void {
    this.dialog.open(ModalAddTaskComponent, {
      width: '800px',
      enterAnimationDuration,
      exitAnimationDuration,
      data: {
        projectId: this.id
      }
    });
  }
}
