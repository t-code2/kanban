import { Component } from '@angular/core';
import { AppService } from 'src/app/app.service'
import { Project } from 'src/app/app.types'

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent {
  projects: Project[] = []

  searchString: string = '';

  private allProjects: Project[] = [];

  constructor(private appService: AppService) {}

  async ngAfterViewInit() {
    this.allProjects = await this.appService.getProjects()
    this.updateSearch();
  }

  updateSearch() {
    this.projects = this.allProjects.filter(project => {
      if (!this.searchString) return true;
      return project.title.toLowerCase().includes(this.searchString.toLowerCase());
    })
  }
}
