const transformTaskStatus = (statusCode: string): string => {
  switch (statusCode) {
    case '2': return 'Ждёт выполнения';
    case '3': return 'Выполняется';
    case '4': return 'Ожидает контроля';
    case '5': return 'Завершена';
    case '6': return 'Отложена';
    default: return '';
  }  
}

const transformTaskStatusToNumber = (statusName: string): string => {
  switch (statusName) {
    case 'Ждёт выполнения': return '2';
    case 'Выполняется': return '3';
    case 'Ожидает контроля': return '4';
    case 'Завершена': return '5';
    case 'Отложена': return '6';
    default: return '';
  }  
}

export {
  transformTaskStatus,
  transformTaskStatusToNumber
};