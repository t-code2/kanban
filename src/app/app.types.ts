export interface BitrixAnswer {
  next: number;
  result: any[];
  total: number;
}

export interface Project {
  id: string;
  title: string;
}

export interface Task {
  id: string;
  title: string;
  description: string;
  status: string;
  responsible: string;
  projectName: string;
}

export interface CommentTask {
  author: string;
  message: string;
  date: Date;
}

export interface CreateCommentObject {
  POST_MESSAGE: string;
}

export interface UpdateTaskObject {
  TITLE?: string;
  DESCRIPTION?: string;
  RESPONSIBLE_ID?: string;
  GROUP_ID?: number;
  CREATOR_ID?: number;
  STATUS?: string;
  PRIORITY?: string; 
}

export interface User {
  id: string;
  name: string;
}

export interface CreateTaskObject {
  TITLE: string;
  GROUP_ID: string;

  DESCRIPTION?: string;
  RESPONSIBLE_ID?: string;
}