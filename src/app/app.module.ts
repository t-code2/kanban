import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainViewComponent } from './pages/main-view/main-view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatCardModule} from '@angular/material/card'
import { CardTaskComponent } from './card-task/card-task.component';
import { MatDialogModule } from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import { HttpClientModule } from '@angular/common/http';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { ProjectsComponent } from './pages/projects/projects.component';
import { ProjectCardComponent } from './project-card/project-card.component'
import { TruncatePipe } from './pipes/truncate.pipe'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DialogAnimationsExampleDialog } from './card-task/card-full.component';
import { ModalAddTaskComponent } from './modal-add-task/modal-add-task.component';

import { SocketIoModule } from 'ngx-socket-io';
import { config } from './socket.config';

@NgModule({
  declarations: [
    AppComponent,
    MainViewComponent,
    CardTaskComponent,
    ProjectsComponent,
    ProjectCardComponent,
    DialogAnimationsExampleDialog,
    TruncatePipe,
    ModalAddTaskComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    DragDropModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
    FormsModule,
    MatInputModule,
    MatIconModule,
    MatDividerModule,
    MatProgressBarModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
