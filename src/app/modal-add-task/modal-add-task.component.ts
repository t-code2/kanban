import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { map, Observable, startWith } from 'rxjs'
import { AppService } from '../app.service'
import { User } from '../app.types'

@Component({
  selector: 'app-modal-add-task',
  templateUrl: './modal-add-task.component.html',
  styleUrls: ['./modal-add-task.component.scss']
})
export class ModalAddTaskComponent implements OnInit {
  responsibleControl = new FormControl('');
  options: User[] = [];
  filteredOptions: Observable<User[]> = new Observable();
  
  taskTitle: string = '';
  taskDescription: string = '';

  private projectId: string = '';

  constructor(
    public dialogRef: MatDialogRef<ModalAddTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private appService: AppService
  ) {

  }

  async ngOnInit() {
    this.projectId = this.data.projectId
    const users = await this.appService.getUsers();
    this.options = users
    this.filteredOptions = this.responsibleControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filter(value || '')),
    );
  }

  async createTask() {
    const userId = this.options.find(user => user.name === this.responsibleControl.value)?.id
    if (!userId) return;

    await this.appService.createTask({
      TITLE: this.taskTitle,
      DESCRIPTION: this.taskDescription,
      RESPONSIBLE_ID: userId.toString(),
      GROUP_ID: this.projectId,
    })
    this.dialogRef.close();
  }

  private filter(value: string): User[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.name.toLowerCase().includes(filterValue));
  }
}
